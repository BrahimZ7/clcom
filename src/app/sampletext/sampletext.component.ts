import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sampletext',
  templateUrl: './sampletext.component.html',
  styleUrls: ['./sampletext.component.css']
})
export class SampletextComponent {
  title = "SampletextComponent"
  status = false

  constructor() { }

  toggleStatus() {
    this.status = !this.status
  }

}
