import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SampletextComponent } from './sampletext/sampletext.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  describe('TaskComponent', () => {
    let component: SampletextComponent;
    let fixture: ComponentFixture<SampletextComponent>;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [SampletextComponent]
      })
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SampletextComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should have SampletextComponent as title and status should be false', () => {
      expect(component.title).toEqual('SampletextComponent');
      expect(component.status).toEqual(false);
    });

    it('should toggle the status', () => {
      component.toggleStatus();
      expect(component.status).toEqual(true);
    });
  });

  it(`should have as title 'CLCOM Angular Application'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('CLCOM Angular Application');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('span')?.textContent).toContain('We are showing a Project which was deployed and built with the help of Gitlabs CI/CD Pipeline');
  });
});
